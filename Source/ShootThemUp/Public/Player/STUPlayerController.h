// SMOG STU Game, All Rights Reserved.

#pragma once

#include "FunctionLibraries/GameDataFunctionLibrary.h"

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "STUPlayerController.generated.h"

class USTURespawnComponent;
class UInputMappingContext;

UCLASS()
class SHOOTTHEMUP_API ASTUPlayerController : public APlayerController
{
    GENERATED_BODY()

    public:
    ASTUPlayerController();
	
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Game")
    USTURespawnComponent* RespawnComponent;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
    class UInputAction* GamePauseAction;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
    UInputMappingContext* PlayerMappingContext;

    virtual void BeginPlay() override;
    virtual void SetupInputComponent() override;
    void CreateMappingContext();

private:
    void OnGamePause(); 
    void OnMatchStateChanged(ESTUMatchState State);
};
