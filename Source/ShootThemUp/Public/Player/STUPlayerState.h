// SMOG STU Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "STUPlayerState.generated.h"

UCLASS()
class SHOOTTHEMUP_API ASTUPlayerState : public APlayerState
{
	GENERATED_BODY()
	//Team
	public:
    void SetTeamID(int32 ID)
    {
        TeamID = ID;
    }
    int32 GetTeamID() const 
    {
        return TeamID;
    }

    void SetTeamColor(const FLinearColor Color)
    {
        TeamColor = Color;
    }
    FLinearColor GetTeamColor()
    {
        return TeamColor;
    }

	private:
    int32 TeamID;
    FLinearColor TeamColor;

    //Stats
public:
    void AddKill()
    {
        ++KillsNum;
    }
    int32 GetKillsNum() const
    {
        return KillsNum;
    }

    void AddDeaths()
    {
        ++DeathsNum;
    }
    int32 GetDeathsNum() const
    {
        return DeathsNum;
    }

    void LogInfo();

    private:
    int32 KillsNum = 0;
    int32 DeathsNum = 0;
};
