// SMOG STU Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"

#include "STUBaseCharacter.generated.h"

//Health
class USTUHealthComponent;
// Input Lib
class UInputMappingContext;
//Weapon
class USTUWeaponComponent;


UCLASS()
class SHOOTTHEMUP_API ASTUBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
    ASTUBaseCharacter(const FObjectInitializer& ObjInit);

protected:
	virtual void BeginPlay() override;

    //Weapon
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    USTUWeaponComponent* WeaponComponent;

public:
	virtual void Tick(float DeltaTime) override;

    UFUNCTION(BlueprintCallable, Category = "Movement")
	virtual bool IsRunning() const;

    UFUNCTION(BlueprintCallable, Category = "Movement")
    float GetMovementDirection() const;

    //Health
private:
    void OnHealthChanged(float Health, float HealthDelta);
protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    USTUHealthComponent* HealthComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
    UAnimMontage* AM_Death;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
    float LifeSpanOnDeath = 5.0f;
    virtual void OnDeath();
    //Fall
protected:
    UPROPERTY(EditDefaultsOnly, Category = "Damage")
    FVector2D LandedDamageVelocity = FVector2D(900.0f, 1200.0f);
    UPROPERTY(EditDefaultsOnly, Category = "Damage")
    FVector2D LandedDamage = FVector2D(10.0f, 100.0f);

private:
    UFUNCTION()
    void OnGroundLanded(const FHitResult& Hit);

    //Team
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Material")
    FName MaterialColorName = "Paint Color";

    void SetPlayerColor(const FLinearColor& Color);
};
