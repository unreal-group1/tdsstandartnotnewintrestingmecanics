// SMOG STU Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/STUBaseWeapon.h"
#include "STURifleWeapon.generated.h"

class USTUWeaponFXComponent;
class UParticleSystem;

UCLASS()
class SHOOTTHEMUP_API ASTURifleWeapon : public ASTUBaseWeapon
{
	GENERATED_BODY()
	
	public:
    ASTURifleWeapon();

    virtual void StartFire() override;
    virtual void StopFire() override;

	protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    float TimeBetweenShots = 0.1f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    float BulletSpread = 1.5f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    float DamageAmount = 100.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon|VFX")
    USTUWeaponFXComponent* WeaponFXComponent;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon|VFX")
    UParticleSystem* TraceFX;

    virtual void BeginPlay() override;
    virtual void MakeShot() override;
    virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const override;

	private:
    FTimerHandle ShotTimerHandle;

    UPROPERTY()
    UParticleSystemComponent* MuzzleFXComponent;

    void MakeDamage(FHitResult HitResult);
    void InitMuzzleFX();
    void SetMuzzleFXVisibility(bool bIsVisibility);
    void SpawnTraceFX(const FVector& TraceStart, const FVector& TraceEnd);

    AController* GetController() const;
};
