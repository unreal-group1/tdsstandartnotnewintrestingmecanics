 // SMOG STU Game, All Rights Reserved.

#pragma once
#include "FunctionLibraries/WeaponFunctionLibrary.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STUBaseWeapon.generated.h"

class USkeletalMeshComponent;
class UParticleSystem;

UCLASS()
class SHOOTTHEMUP_API ASTUBaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	ASTUBaseWeapon();

    FOnClipEmptySignature OnClipEmpty;
    bool IsAmmoEmpty() const;

	virtual void StartFire();
    virtual void StopFire();

    void ChangeClip();
    bool CanReload() const;

    FAmmoData GetAmmoData() const
    {
        return CurrentAmmo;
    }

    FAmmoData GetDefaultAmmoData() const
    {
        return DefaultAmmo;
    }

    FWeaponUIData GetUIData() const
    {
        return UIData;
    }

    bool TryToAddAmmo(int32 ClipsAmount);

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapon")
    USkeletalMeshComponent* WeaponMesh;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    FName MuzzleSocketName;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float TraceMaxDistance = 1500;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    FAmmoData DefaultAmmo{15, 10, false};

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    FWeaponUIData UIData;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon|VFX")
    UParticleSystem* MuzzleFX;

	virtual void BeginPlay() override;
	
	virtual void MakeShot();

    bool GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const;
    FVector GetMuzzleWorldLocation() const;
    virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const;
    void MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TrceEnd);

    void DecreaseAmmo();
    bool IsClipEmpty() const;
    bool IsAmmoFull() const;  

    void LogAmmo();

    UParticleSystemComponent* SpawnMuzzleFX();

    private:
    FAmmoData CurrentAmmo;
};
