// SMOG STU Game, All Rights Reserved.

#pragma once

#include "FunctionLibraries/GameDataFunctionLibrary.h"

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "STUGameHUD.generated.h"


UCLASS()
class SHOOTTHEMUP_API ASTUGameHUD : public AHUD
{
	GENERATED_BODY()
	
	public:
    virtual void DrawHUD() override;

	protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UUserWidget> PlayerHUDWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UUserWidget> PauseWidgetClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UUserWidget> GameOverWidgetClass;

	virtual void BeginPlay() override;

	private: 
	UPROPERTY()
    TMap<ESTUMatchState, UUserWidget*> GameWidgets;
    UPROPERTY()
    UUserWidget* CurrentWidget = nullptr;

	void DrawCrossHeir();
    void OnMatchStateChanged(ESTUMatchState State);
    void HideCurrentWidget();
    void ShowStateWidget(ESTUMatchState State);
};
