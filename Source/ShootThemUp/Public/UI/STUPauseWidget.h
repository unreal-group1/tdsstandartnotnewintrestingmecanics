// SMOG STU Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "STUPauseWidget.generated.h"

class UButton;

UCLASS()
class SHOOTTHEMUP_API USTUPauseWidget : public UUserWidget
{
	GENERATED_BODY()
	
	protected:
    UPROPERTY(meta = (BindWidget))
    UButton* UnpauseButton;

	virtual void NativeOnInitialized() override;

	private:
    UFUNCTION()
    void Unpause();
};
