// SMOG STU Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "STUAIController.generated.h"

class USTUAIPerceptionComponent;
class USTURespawnComponent;

UCLASS()
class SHOOTTHEMUP_API ASTUAIController : public AAIController
{
	GENERATED_BODY()

	public:
    ASTUAIController();
	
	protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AI")
    USTUAIPerceptionComponent* STUAIPreceptionComponent;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Game")
    USTURespawnComponent* RespawnComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    FName FocusOnKeyName = "EnemyActor";

    virtual void OnPossess(APawn* InPawn) override;
    virtual void Tick(float DeltaTime) override;

    private:
    AActor* GetFocusOnActor() const;
};
