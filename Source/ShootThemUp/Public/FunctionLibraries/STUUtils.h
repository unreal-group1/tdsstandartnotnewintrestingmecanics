// SMOG STU Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "STUUtils.generated.h"


template <typename T> static T* FindFirstNotifyByClass(UAnimSequenceBase* Animation)
{
    if (!Animation)
    {
        return nullptr;
    }

    const auto NotifyEvents = Animation->Notifies;
    for (auto NotifyEvent : NotifyEvents)
    {
        auto AnimNotify = Cast<T>(NotifyEvent.Notify);
        if (AnimNotify)
        {
            return AnimNotify;
        }
    }

    return nullptr;
}

template <typename T> static T* GetSTUPlayerComponent(AActor* Player)
{
    if (!Player)
    {
        return nullptr;
    }

    const auto Component = Player->GetComponentByClass(T::StaticClass());

    return Cast<T>(Component);
}

static FText TextFromInt(int32 Number)
{
    return FText::FromString(FString::FromInt(Number));
}


UCLASS()
class SHOOTTHEMUP_API USTUUtils : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
