// SMOG STU Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "Player/STUPlayerState.h"
#include "EnemyFunctionLibrary.generated.h"

UCLASS()
class SHOOTTHEMUP_API UEnemyFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
	public:
    static bool TheyAreEnemies(AController* Controller1, AController* Controller2);
};
