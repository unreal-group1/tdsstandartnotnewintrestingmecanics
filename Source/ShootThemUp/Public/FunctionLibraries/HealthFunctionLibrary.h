// SMOG STU Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "HealthFunctionLibrary.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnDeathSignature);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnHealthChangedSignature, float, float);

UCLASS()
class SHOOTTHEMUP_API UHealthFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
