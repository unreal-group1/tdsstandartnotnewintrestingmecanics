// SMOG STU Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "CTUCharacterMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTTHEMUP_API UCTUCharacterMovementComponent : public UCharacterMovementComponent
{
    GENERATED_BODY()

    public:
    virtual float GetMaxSpeed() const override;

    UPROPERTY(EditDefaultsOnly, 
        BlueprintReadWrite, 
        Category = "Movement", 
        meta = (ClampMin = "1.0", ClampMax = "10.0"))
    float RunModifier = 1.4f;
	
};
