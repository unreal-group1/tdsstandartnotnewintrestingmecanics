// SMOG STU Game, All Rights Reserved.

#pragma once
#include "FunctionLibraries/HealthFunctionLibrary.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "STUHealthComponent.generated.h"

class UCameraShakeBase;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTTHEMUP_API USTUHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	USTUHealthComponent();

    FOnDeathSignature OnDeath;
    FOnHealthChangedSignature OnHealthChanged; 

	float GetHealth()
    {
        return Health;
    }
    UFUNCTION(BlueprintCallable, Category = "Health")
    float GetHealthPercent() const
    {
        return Health / MaxHealth;
    }

	UFUNCTION(BlueprintCallable, Category = "Health")
	bool IsDead() const
	{
        return FMath::IsNearlyZero(Health);
	}

    bool TryToAddHealth(float HealthAmount);
    bool IsHealthFull() const;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, 
		BlueprintReadWrite, 
		Category = "Health",
        meta = (ClampMin = "0.0", ClampMax = "999999999.0"))
    float MaxHealth = 0.0f;

private:	
	float Health = 0.0f;

	// Damage
    UFUNCTION()
    void OnTakeAnyDamageHandle(
        AActor* DamageActor, 
		float Damage, 
		const class UDamageType* DamageType, 
		class AController* InstigatedBy, 
		AActor* DamageCauser);

	//Heal
protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health|Heal")
    bool AutoHeal = true;
    UPROPERTY(
        EditDefaultsOnly, 
        BlueprintReadWrite, 
        Category = "Health|Heal", 
        meta = (EditCondition = "AutoHeal"))
    float HealUpdateTime = 1.0f;
    UPROPERTY(
        EditDefaultsOnly, 
        BlueprintReadWrite, 
        Category = "Health|Heal", 
        meta = (EditCondition = "AutoHeal")) 
    float HealDelay = 3.0f;
    UPROPERTY(
        EditDefaultsOnly, 
        BlueprintReadWrite, 
        Category = "Health|Heal", 
        meta = (EditCondition = "AutoHeal"))
    float HealModifier = 1.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health|VFX")
    TSubclassOf<UCameraShakeBase> CameraShake;

private:
    FTimerHandle HealTimerHandle;
    void HealUpdate();
    void SetHealth(float NewHealth);

    void PlayCameraShake();

    void Killed(AController* KillerController);
};
 