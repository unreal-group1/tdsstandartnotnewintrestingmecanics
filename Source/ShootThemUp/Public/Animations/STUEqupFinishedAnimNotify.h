// SMOG STU Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Animations/STUAnimNotify.h"
#include "STUEqupFinishedAnimNotify.generated.h"

UCLASS()
class SHOOTTHEMUP_API USTUEqupFinishedAnimNotify : public USTUAnimNotify
{
	GENERATED_BODY()

};
