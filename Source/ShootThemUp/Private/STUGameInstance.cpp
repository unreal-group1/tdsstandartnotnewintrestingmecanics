// SMOG STU Game, All Rights Reserved.


#include "STUGameInstance.h"

FName USTUGameInstance::GetStartupLevelName() const
{
    return StartupLevelName;
}

FName USTUGameInstance::GetMenuLevelName() const
{
    return MenuLevelName;
}
