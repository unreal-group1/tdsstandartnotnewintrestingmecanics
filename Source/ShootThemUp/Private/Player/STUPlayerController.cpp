// SMOG STU Game, All Rights Reserved.


#include "Player/STUPlayerController.h"
#include "STUPlayerController.h"
#include "Components/STURespawnComponent.h"
#include "STUGameModeBase.h"

#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"

ASTUPlayerController::ASTUPlayerController()
{
    RespawnComponent = CreateDefaultSubobject<USTURespawnComponent>("RespawnComponent");
    CreateMappingContext();
}

void ASTUPlayerController::BeginPlay()
{
    Super::BeginPlay();

    if (!GetWorld())
    {
        return;
    }

    const auto GameMode = Cast<ASTUGameModeBase>(GetWorld()->GetAuthGameMode());
    if (!GameMode)
    {
        return;
    }
    GameMode->OnMatchStateChanged.AddUObject(this, &ASTUPlayerController::OnMatchStateChanged);
}

void ASTUPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent);
    if (!EnhancedInputComponent)
    {
        return;
    }
    EnhancedInputComponent->BindAction(GamePauseAction, ETriggerEvent::Triggered, this, &ASTUPlayerController::OnGamePause);
}

void ASTUPlayerController::CreateMappingContext()
{
    UEnhancedInputLocalPlayerSubsystem* EnhancedInputSubsystem =
        ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer());
    if (!EnhancedInputSubsystem)
    {
        return;
    }

    EnhancedInputSubsystem->AddMappingContext(PlayerMappingContext, 0);
}

void ASTUPlayerController::OnGamePause()
{
    if (!GetWorld() 
        || !GetWorld()->GetAuthGameMode())
    {
        return;
    }

    GetWorld()->GetAuthGameMode()->SetPause(this);
}

void ASTUPlayerController::OnMatchStateChanged(ESTUMatchState State)
{
    if (State == ESTUMatchState::InProgress)
    {
        SetInputMode(FInputModeGameOnly());
        bShowMouseCursor = false;
    }
    else
    {
        SetInputMode(FInputModeUIOnly());
        bShowMouseCursor = true;
    }
}
