// SMOG STU Game, All Rights Reserved.

#include "Player/STUPlayerCharacter.h"
#include "Player/STUPlayerController.h"
#include "Components/STUWeaponComponent.h"

#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/PrimitiveComponent.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "GameFramework/Controller.h"

ASTUPlayerCharacter::ASTUPlayerCharacter(const FObjectInitializer& ObjInit)
    : Super(ObjInit) 
{
    PrimaryActorTick.bCanEverTick = true;

    SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>("SpringArmComponent");
    SpringArmComponent->SetupAttachment(GetRootComponent());
    SpringArmComponent->bUsePawnControlRotation = true;
    SpringArmComponent->SocketOffset = FVector(0.0f, 100.0f, 0.0f);

    CameraComponent = CreateDefaultSubobject<UCameraComponent>("CameraComponent");
    CameraComponent->SetupAttachment(SpringArmComponent);

    CameraCollisonComponent = CreateDefaultSubobject<USphereComponent>("CameraCollisonComponent");
    CameraCollisonComponent->SetupAttachment(CameraComponent);
    CameraCollisonComponent->SetSphereRadius(10.0f);
    CameraCollisonComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
}

void ASTUPlayerCharacter::BeginPlay()
{
    Super::BeginPlay();

    const auto PlayerController = Cast<ASTUPlayerController>(GetController());
    if (!PlayerController)
    {
        return;
    }
    PlayerController->CreateMappingContext();

    check(CameraCollisonComponent);

    CameraCollisonComponent->OnComponentBeginOverlap.AddDynamic(this, &ASTUPlayerCharacter::OnCameraCollisionBeginOverlap);
    CameraCollisonComponent->OnComponentEndOverlap.AddDynamic(this, &ASTUPlayerCharacter::OnCameraCollisionEndOverlap);
}
//Inputs
void ASTUPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    check(PlayerInputComponent);
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    check(WeaponComponent);

    UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent);
    if (!EnhancedInputComponent)
    {
        return;
    }

    EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ASTUPlayerCharacter::Move);
    EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ASTUPlayerCharacter::Look);
    EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ASTUPlayerCharacter::Jump);
    EnhancedInputComponent->BindAction(RunAction, ETriggerEvent::Started, this, &ASTUPlayerCharacter::OnStartRunning);
    EnhancedInputComponent->BindAction(RunAction, ETriggerEvent::Completed, this, &ASTUPlayerCharacter::OnStopRunning);
    EnhancedInputComponent->BindAction(FireAction, ETriggerEvent::Started, WeaponComponent, &USTUWeaponComponent::StartFire);
    EnhancedInputComponent->BindAction(FireAction, ETriggerEvent::Completed, WeaponComponent, &USTUWeaponComponent::StopFire);
    EnhancedInputComponent->BindAction(NextWeaponAction, ETriggerEvent::Triggered, WeaponComponent, &USTUWeaponComponent::NextWeapon);
    EnhancedInputComponent->BindAction(ReloadAction, ETriggerEvent::Triggered, WeaponComponent, &USTUWeaponComponent::Reload);
}

//Moving
void ASTUPlayerCharacter::Move(const FInputActionValue& Value)
{
    const FVector2D MoveValue = Value.Get<FVector2D>();

    if (MoveValue.IsZero())
    {
        return;
    }

    AddMovementInput(GetActorForwardVector(), MoveValue.X);
    AddMovementInput(GetActorRightVector(), MoveValue.Y);
    //UE_LOG(LogTemp, Display, TEXT("Move.X %f, Move.Y %f"), MoveValue.X, MoveValue.Y)

    if (MoveValue.X > 0)
    {
        bIsMovingForward = true;
    }
    else
    {
        bIsMovingForward = false;
    }
}

void ASTUPlayerCharacter::Look(const FInputActionValue& Value)
{
    const FVector2D LookValue = Value.Get<FVector2D>();

    if (LookValue.IsZero())
    {
        return;
    }

    AddControllerPitchInput(LookValue.X);
    AddControllerYawInput(LookValue.Y);
}

void ASTUPlayerCharacter::OnStartRunning()
{
    bWantsToRun = true;
}

void ASTUPlayerCharacter::OnStopRunning()
{
    bWantsToRun = false;
}

bool ASTUPlayerCharacter::IsRunning() const
{
    if (bWantsToRun && bIsMovingForward && !GetVelocity().IsZero())
    {
        return true;
    }
    else
    {
        return false;
    }
}

//Health
void ASTUPlayerCharacter::OnDeath()
{
    Super::OnDeath();

    if (!Controller)
    {
        return;
    }
    Controller->ChangeState(NAME_Spectating);
}

//Camera
void ASTUPlayerCharacter::OnCameraCollisionBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    CheckCameraOverlap();
}

void ASTUPlayerCharacter::OnCameraCollisionEndOverlap(
    UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
    CheckCameraOverlap();
}

void ASTUPlayerCharacter::CheckCameraOverlap()
{
    const auto bIsHideMesh = CameraCollisonComponent->IsOverlappingComponent(GetCapsuleComponent());
    GetMesh()->SetOwnerNoSee(bIsHideMesh);

    TArray<USceneComponent*> MeshChildrens;
    GetMesh()->GetChildrenComponents(true, MeshChildrens);

    for (auto MeshChild : MeshChildrens)
    {
        const auto MeshCildGeometry = Cast<UPrimitiveComponent>(MeshChild);
        if (MeshCildGeometry)
        {
            MeshCildGeometry->SetOwnerNoSee(bIsHideMesh);
        }
    }
}
