// SMOG STU Game, All Rights Reserved.

#include "AI/Decorators/STUAmmoPercentDecorator.h"
#include "FunctionLibraries/STUUtils.h"
#include "Components/STUHealthComponent.h"
#include "Components/STUWeaponComponent.h"

#include "AIController.h"

USTUAmmoPercentDecorator::USTUAmmoPercentDecorator()
{
    NodeName = "Need Ammo";
}

bool USTUAmmoPercentDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
    const auto Controller = OwnerComp.GetAIOwner();
    if (!Controller)
    {
        return false;
    }

    const auto HealthComponent = GetSTUPlayerComponent<USTUHealthComponent>(Controller->GetPawn());
    const auto WeaponComponent = GetSTUPlayerComponent<USTUWeaponComponent>(Controller->GetPawn());
    if (!HealthComponent 
        || !WeaponComponent 
        || HealthComponent->IsDead())
    {
        return false;
    }

    float MaxCountBullets = (float)WeaponComponent->GetMaxCountBullets(WeaponType);
    float CountBullets = (float)WeaponComponent->GetCountBullets(WeaponType);
    if (MaxCountBullets <= 0 
        || CountBullets <= 0)
    {
        return false;
    }
    float CurrentAmmoPercent = CountBullets / MaxCountBullets;
    //UE_LOG(LogTemp, Display, TEXT("CountBullets/MaxCountBullets = %f"), CurrentAmmoPercent);
    return CurrentAmmoPercent <= AmmoPercent;
}
