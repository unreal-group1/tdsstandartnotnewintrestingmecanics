// SMOG STU Game, All Rights Reserved.

#include "AI/Components/STUAIPerceptionComponent.h"
#include "FunctionLibraries/STUUtils.h"
#include "FunctionLibraries/EnemyFunctionLibrary.h"
#include "Components/STUHealthComponent.h"


#include "AIController.h"
#include "Perception/AISense_Sight.h"

AActor* USTUAIPerceptionComponent::GetClosestEnemy() const
{
    TArray<AActor*> PercieveActors;
    GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PercieveActors);

    if (PercieveActors.Num() == 0)
    {
        return nullptr;
    }

    const auto Controller = Cast<AAIController>(GetOwner());
    if (!Controller)
    {
        return nullptr;
    }

    const auto Pawn = Controller->GetPawn();
    if (!Pawn)
    {
        return nullptr;
    }

    float BestDistance = MAX_FLT;
    AActor* BastPawn = nullptr;
    for (const auto PercieveActor : PercieveActors)
    {
        const auto PercievePawn = Cast<APawn>(PercieveActor);
        const auto AreEnemies = PercievePawn && UEnemyFunctionLibrary::TheyAreEnemies(Controller, PercievePawn->GetController());
        const auto HealthComponent = GetSTUPlayerComponent<USTUHealthComponent>(PercieveActor);
        if (!HealthComponent
            || HealthComponent->IsDead() 
            || !AreEnemies) 
        {
            continue;
        }

        const auto CurrentDistance = (PercieveActor->GetActorLocation() - Pawn->GetActorLocation()).Size();
        if (CurrentDistance < BestDistance)
        {
            BestDistance = CurrentDistance;
            BastPawn = PercieveActor;
        }
    }

    return BastPawn;
}
