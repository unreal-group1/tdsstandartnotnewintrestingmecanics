// SMOG STU Game, All Rights Reserved.


#include "AI/Services/STUFireService.h"
#include "AI/Components/STUAIPerceptionComponent.h"
#include "FunctionLibraries/STUUtils.h"
#include "Components/STUWeaponComponent.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"

USTUFireService::USTUFireService()
{
    NodeName = "Fire";
}

void USTUFireService::TickNode(
    UBehaviorTreeComponent& OwnerComp, 
    uint8* NodeMemory, 
    float DeltaSeconds)
{
    const auto Blackboard = OwnerComp.GetBlackboardComponent();
    const auto Controller = OwnerComp.GetAIOwner();

    const auto HasAim = Blackboard && Blackboard->GetValueAsObject(EnemyActorKey.SelectedKeyName);
    if (Controller)
    {
        const auto WeaponComponent =
            GetSTUPlayerComponent<USTUWeaponComponent>(Controller->GetPawn());
        if (WeaponComponent)
        {
            HasAim ? WeaponComponent->StartFire() : WeaponComponent->StopFire();
        }

    }

    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
