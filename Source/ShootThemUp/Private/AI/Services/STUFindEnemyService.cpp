// SMOG STU Game, All Rights Reserved.


#include "AI/Services/STUFindEnemyService.h"
#include "AI/Components/STUAIPerceptionComponent.h"
#include "FunctionLibraries/STUUtils.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"

USTUFindEnemyService::USTUFindEnemyService()
{
    NodeName = "Find Enemy";
}

void USTUFindEnemyService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
    const auto Blackboard = OwnerComp.GetBlackboardComponent();
    if (Blackboard)
    {

        const auto Controller = OwnerComp.GetAIOwner();
        const auto PerceptionComponent = GetSTUPlayerComponent<USTUAIPerceptionComponent>(Controller);
        if (PerceptionComponent)
        {
            Blackboard->SetValueAsObject(EnemyActorKey.SelectedKeyName, PerceptionComponent->GetClosestEnemy());
        }
    }

    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
