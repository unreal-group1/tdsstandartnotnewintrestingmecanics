// SMOG STU Game, All Rights Reserved.


#include "AI/STUAICharacter.h"
#include "AI/STUAIController.h"
#include "Components/STUAIWeaponComponent.h"

#include "BrainComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

ASTUAICharacter::ASTUAICharacter(const FObjectInitializer& ObjInit) 
: Super(ObjInit.SetDefaultSubobjectClass<USTUAIWeaponComponent>("WeaponComponent"))
{
    AutoPossessAI = EAutoPossessAI::Disabled;
    AIControllerClass = ASTUAIController::StaticClass();

    bUseControllerRotationYaw = false;

    if (!GetCharacterMovement())
    {
        return;
    }
    GetCharacterMovement()->bUseControllerDesiredRotation = true;
    GetCharacterMovement()->RotationRate = FRotator(0.0f, 180.0f, 0.0f);

}

void ASTUAICharacter::OnDeath()
{ 
    Super::OnDeath();

    const auto STUController = Cast<AAIController>(Controller);
    if (STUController
        && STUController->BrainComponent)
    {
        STUController->BrainComponent->Cleanup();
    }
}
