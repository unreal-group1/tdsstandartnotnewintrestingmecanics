// SMOG STU Game, All Rights Reserved.


#include "Components/STUHealthComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Controller.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Camera/CameraShakeBase.h"

#include "Dev/STUFireDamageType.h"
#include "Dev/STUIceDamageType.h"
#include "STUGameModeBase.h"

DEFINE_LOG_CATEGORY_STATIC(HealthLog, All, All);

USTUHealthComponent::USTUHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

bool USTUHealthComponent::TryToAddHealth(float HealthAmount)
{
	if (IsDead() 
		|| IsHealthFull())
	{
        return false;
	}

	SetHealth(Health + HealthAmount);
    return true;
}

bool USTUHealthComponent::IsHealthFull() const
{
    return FMath::IsNearlyEqual(Health, MaxHealth);
}

void USTUHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	check(MaxHealth > 0);

	SetHealth(MaxHealth);

	AActor* ComponentOwner = GetOwner();
	if (!ComponentOwner)
	{
        return;
	}

    ComponentOwner->OnTakeAnyDamage.AddDynamic(
		this, 
		&USTUHealthComponent::OnTakeAnyDamageHandle
	);
}

void USTUHealthComponent::OnTakeAnyDamageHandle(
    AActor* DamageActor, 
	float Damage, 
	const UDamageType* DamageType, 
	AController* InstigatedBy, 
	AActor* DamageCauser)
{
    if (FMath::IsNearlyZero(Health) 
		|| !GetWorld())
	{
        return;
	}

	SetHealth(Health - Damage);

	if (IsDead())
	{
        Killed(InstigatedBy);
        OnDeath.Broadcast();
	}
	else 
	if (AutoHeal)
	{
        GetWorld()->GetTimerManager().SetTimer(
			HealTimerHandle, 
			this, 
			&USTUHealthComponent::HealUpdate,
			HealUpdateTime,
			true,
			HealDelay
		);
	}

	PlayCameraShake();
}

void USTUHealthComponent::HealUpdate()
{
    if (!GetWorld())
    {
        return;
    }

    if (IsHealthFull()
		|| IsDead())
	{
        GetWorld()->GetTimerManager().ClearTimer(HealTimerHandle);
        return;
	}

	SetHealth(Health + HealModifier);
}

void USTUHealthComponent::SetHealth(float NewHealth)
{
    const auto NextHealth = FMath::Clamp(NewHealth, 0.0f, MaxHealth);
    const auto HealthDelta = NextHealth - Health;
    Health = NextHealth;

    OnHealthChanged.Broadcast(Health, HealthDelta);
}

void USTUHealthComponent::PlayCameraShake()
{
	if (IsDead())
	{
        return;
	}

	const auto Player = Cast<APawn>(GetOwner());
    if (!Player)
    {
        return;
    }

	const auto Controller = Player->GetController<APlayerController>();
    if (!Controller 
		|| !Controller->PlayerCameraManager)
    {
        return;
    }
    Controller->PlayerCameraManager->StartCameraShake(CameraShake);
}

void USTUHealthComponent::Killed(AController* KillerController)
{
    if (!GetWorld())
    {
        return;
    }

    const auto GameMode = Cast<ASTUGameModeBase>(GetWorld()->GetAuthGameMode());
	if (!GameMode)
	{
        return;
	}

	const auto Player = Cast<APawn>(GetOwner());
    const auto VictimController = Player ? Player->Controller : nullptr;

	GameMode->Killed(KillerController, VictimController);
}
