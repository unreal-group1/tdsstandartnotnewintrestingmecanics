// SMOG STU Game, All Rights Reserved.


#include "Components/CTUCharacterMovementComponent.h"
#include "Player/STUBaseCharacter.h"

float UCTUCharacterMovementComponent::GetMaxSpeed() const
{
    const float MaxSpeed = Super::GetMaxSpeed();
    const ASTUBaseCharacter* Player = Cast<ASTUBaseCharacter>(GetPawnOwner());

    return Player 
        && Player->IsRunning() ? MaxSpeed * RunModifier : MaxSpeed;
}
