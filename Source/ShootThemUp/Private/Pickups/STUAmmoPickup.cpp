// SMOG STU Game, All Rights Reserved.

#include "Pickups/STUAmmoPickup.h"
#include "Components/STUWeaponComponent.h"
#include "Components/STUHealthComponent.h"
#include "FunctionLibraries/STUUtils.h"

bool ASTUAmmoPickup::GivePickupTo(APawn* PlayerPawn)
{
    const auto HealthComponent = GetSTUPlayerComponent<USTUHealthComponent>(PlayerPawn);
    if (!HealthComponent || 
        HealthComponent->IsDead())
    {
        return false;
    }

    const auto WeaponComponent = GetSTUPlayerComponent<USTUWeaponComponent>(PlayerPawn);
    if (!WeaponComponent)
    {
        return false;
    }

    return WeaponComponent->TryToAddAmmo(WeaponType, ClipsAmount);
}
