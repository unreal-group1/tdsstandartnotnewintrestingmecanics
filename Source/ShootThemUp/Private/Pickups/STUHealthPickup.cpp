// SMOG STU Game, All Rights Reserved.


#include "Pickups/STUHealthPickup.h"
#include "Components/STUWeaponComponent.h"
#include "Components/STUHealthComponent.h"
#include "FunctionLibraries/STUUtils.h"

bool ASTUHealthPickup::GivePickupTo(APawn* PlayerPawn)
{
    const auto HealthComponent = GetSTUPlayerComponent<USTUHealthComponent>(PlayerPawn);
    if (!HealthComponent)
    {
        return false;
    }

    return HealthComponent->TryToAddHealth(HealthAmount);
}
