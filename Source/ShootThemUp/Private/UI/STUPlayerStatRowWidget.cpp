// SMOG STU Game, All Rights Reserved.

#include "UI/STUPlayerStatRowWidget.h"

#include "Components/TextBlock.h"
#include "Components/Image.h"

 void USTUPlayerStatRowWidget::SetPlayerName(const FText& Text)
{
     if (!PlayerNameTextBlock)
     {
         return;
     }
     PlayerNameTextBlock->SetText(Text);
 }

void USTUPlayerStatRowWidget::SetKills(const FText& Text)
{
     if (!KillsTextBlock)
     {
         return;
     }
     KillsTextBlock->SetText(Text);
}

void USTUPlayerStatRowWidget::SetDeathes(const FText& Text)
{
     if (!DeathesTextBlock)
     {
         return;
     }
     DeathesTextBlock->SetText(Text);
}

void USTUPlayerStatRowWidget::SetTeam(const FText& Text)
{
     if (!TeamTextBlock)
     {
         return;
     }
     TeamTextBlock->SetText(Text);
}

void USTUPlayerStatRowWidget::SetPlayerIndicatorVisibility(bool bIsVisible)
{
     if (!PlayerIndicatorImage)
     {
         return;
     }
     PlayerIndicatorImage->SetVisibility(bIsVisible ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
}
