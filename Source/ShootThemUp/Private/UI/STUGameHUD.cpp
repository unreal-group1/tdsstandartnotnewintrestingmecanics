// SMOG STU Game, All Rights Reserved.


#include "UI/STUGameHUD.h"
#include "STUGameModeBase.h"

#include "Engine/Canvas.h"
#include "Blueprint/UserWidget.h"

DEFINE_LOG_CATEGORY_STATIC(LogSTUGameHUD, All, All);

void ASTUGameHUD::DrawHUD()
{
    Super::DrawHUD();

    //DrawCrossHeir();
}


void ASTUGameHUD::BeginPlay()
{
    Super::BeginPlay();

    GameWidgets.Add(ESTUMatchState::InProgress, CreateWidget<UUserWidget>(GetWorld(), PlayerHUDWidgetClass));
    GameWidgets.Add(ESTUMatchState::Pause, CreateWidget<UUserWidget>(GetWorld(), PauseWidgetClass));
    GameWidgets.Add(ESTUMatchState::GameOver, CreateWidget<UUserWidget>(GetWorld(), GameOverWidgetClass));

    for (auto GameWidgetPair : GameWidgets)
    {
        const auto GameWidget = GameWidgetPair.Value;
        if (!GameWidget)
        {
            continue;
        }
        GameWidget->AddToViewport();
        GameWidget->SetVisibility(ESlateVisibility::Hidden);
    }

    if (!GetWorld())
    {
        return;
    }

    const auto GameMode = Cast<ASTUGameModeBase>(GetWorld()->GetAuthGameMode());
    if (!GameMode)
    {
        return;
    }
    GameMode->OnMatchStateChanged.AddUObject(this, &ASTUGameHUD::OnMatchStateChanged);
}

void ASTUGameHUD::DrawCrossHeir()
{
    const TInterval<float> Center(Canvas->SizeX * 0.5, Canvas->SizeY * 0.5);
    
    const float HalfLineSize = 10.0f;
    const float LineThickness = 2.0f;
    const FLinearColor LineColor = FLinearColor::Green;

    DrawLine(
        Center.Min - HalfLineSize, 
        Center.Max, 
        Center.Min + HalfLineSize, 
        Center.Max, 
        LineColor, 
        LineThickness);
    DrawLine(
        Center.Min, 
        Center.Max - HalfLineSize, 
        Center.Min, 
        Center.Max + HalfLineSize, 
        LineColor, 
        LineThickness);
}

void ASTUGameHUD::OnMatchStateChanged(ESTUMatchState State)
{
    HideCurrentWidget();
    ShowStateWidget(State);

    UE_LOG(LogSTUGameHUD, Display, TEXT("Match state changed: %s"), *UEnum::GetValueAsString(State));
}

void ASTUGameHUD::HideCurrentWidget()
{
    if (!CurrentWidget)
    {
        return;
    }
    CurrentWidget->SetVisibility(ESlateVisibility::Hidden);
}

void ASTUGameHUD::ShowStateWidget(ESTUMatchState State)
{
    if (GameWidgets.Contains(State))
    {
        CurrentWidget = GameWidgets[State];
    }

    if (!CurrentWidget)
    {
        return;
    }
    CurrentWidget->SetVisibility(ESlateVisibility::Visible);
}
