// SMOG STU Game, All Rights Reserved.


#include "UI/STUGameOverWidget.h"
#include "STUGameModeBase.h"
#include "Player/STUPlayerState.h" 
#include "UI/STUPlayerStatRowWidget.h" 
#include "FunctionLibraries/STUUtils.h"

#include "Components/VerticalBox.h" 
#include "Components/Button.h" 
#include "Kismet/GameplayStatics.h" 

void USTUGameOverWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (!GetWorld())
    {
        return;
    }

    const auto GameMode = Cast<ASTUGameModeBase>(GetWorld()->GetAuthGameMode());
    if (!GameMode)
    {
        return;
    }
    GameMode->OnMatchStateChanged.AddUObject(this, &USTUGameOverWidget::OnMatchStateChanged);

    if (!ResetLevelButton)
    {
        return;
    }
    ResetLevelButton->OnClicked.AddDynamic(this, &USTUGameOverWidget::OnResetLevel);
}

void USTUGameOverWidget::OnMatchStateChanged(ESTUMatchState State)
{
    if (State == ESTUMatchState::GameOver)
    {
        UpdatePlayersState();
    }
}

void USTUGameOverWidget::UpdatePlayersState()
{
    if (!GetWorld() 
        || !PlayerStatBox)
    {
        return;
    }

    PlayerStatBox->ClearChildren();

    for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
    {
        const auto Controller = It->Get();
        if (!Controller)
        {
            continue;
        }
        const auto PlayerState = Cast<ASTUPlayerState>(Controller->PlayerState);
        if (!PlayerState)
        {
            continue;
        }
        const auto PlayerStateRowWidget = CreateWidget<USTUPlayerStatRowWidget>(GetWorld(), PlayerStatRowWidgetClass);
        if (!PlayerStateRowWidget)
        {
            continue;
        }
        PlayerStateRowWidget->SetPlayerName(FText::FromString(PlayerState->GetPlayerName()));
        PlayerStateRowWidget->SetKills(TextFromInt(PlayerState->GetKillsNum()));
        PlayerStateRowWidget->SetDeathes(TextFromInt(PlayerState->GetDeathsNum()));
        PlayerStateRowWidget->SetTeam(TextFromInt(PlayerState->GetTeamID()));
        PlayerStateRowWidget->SetPlayerIndicatorVisibility(Controller->IsPlayerController());

        PlayerStatBox->AddChild(PlayerStateRowWidget);
    }
}

void USTUGameOverWidget::OnResetLevel()
{
    UGameplayStatics::OpenLevel(this, FName(UGameplayStatics::GetCurrentLevelName(this)));
}
