// SMOG STU Game, All Rights Reserved.

#include "UI/STUSpectatorWidget.h"
#include "Components/STURespawnComponent.h"
#include "FunctionLibraries/STUUtils.h"

bool USTUSpectatorWidget::GetRespawnTime(int32& CountDownTime) const
{
    USTURespawnComponent* RespawnComponent = GetSTUPlayerComponent<USTURespawnComponent>(GetOwningPlayer());
    if (!RespawnComponent 
        || !RespawnComponent->IsRespawnInProgress())
    {
        return false;
    }

    CountDownTime = RespawnComponent->GetRespawnCountDown();
    return true;
}
