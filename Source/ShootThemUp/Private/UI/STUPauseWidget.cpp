// SMOG STU Game, All Rights Reserved.

#include "UI/STUPauseWidget.h"

#include "GameFramework/GameModeBase.h"
#include "Components/Button.h"

void USTUPauseWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (!UnpauseButton)
    {
        return;
    }
    UnpauseButton->OnClicked.AddDynamic(this, &USTUPauseWidget::Unpause);
}

void USTUPauseWidget::Unpause()
{
    if (!GetWorld() &&
        !GetWorld()->GetAuthGameMode())
    {
        return;
    }
    GetWorld()->GetAuthGameMode()->ClearPause();
}
 