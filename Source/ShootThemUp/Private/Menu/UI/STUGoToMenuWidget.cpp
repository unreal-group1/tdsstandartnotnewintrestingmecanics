// SMOG STU Game, All Rights Reserved.


#include "Menu/UI/STUGoToMenuWidget.h"
#include "STUGameInstance.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogSTUGoToMenuWidget, All, All);

void USTUGoToMenuWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (!GoToMenuButton)
    {
        return;
    }
    GoToMenuButton->OnClicked.AddDynamic(this, &USTUGoToMenuWidget::OnGoToMenu);
}

void USTUGoToMenuWidget::OnGoToMenu()
{
    if (!GetWorld())
    {
        return;
    }
    const auto STUGameInstatnce = GetWorld()->GetGameInstance<USTUGameInstance>();
    if (!STUGameInstatnce)
    {
        return;
    }
    if (STUGameInstatnce->GetMenuLevelName().IsNone())
    {
        UE_LOG(LogSTUGoToMenuWidget, Error, TEXT("GoToMenu name is NONE"));
        return;
    }

    UGameplayStatics::OpenLevel(this, STUGameInstatnce->GetMenuLevelName());
}