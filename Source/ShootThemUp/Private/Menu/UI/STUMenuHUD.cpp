// SMOG STU Game, All Rights Reserved.

#include "Menu/UI/STUMenuHUD.h"

#include "Blueprint/UserWidget.h"

void ASTUMenuHUD::BeginPlay()
{
    Super::BeginPlay();

    if (!GetWorld() 
        || !MenuWidgetClass)
    {
        return;
    }

    const auto MenuWidget = CreateWidget<UUserWidget>(GetWorld(), MenuWidgetClass);
    if (!MenuWidget)
    {
        return;
    }
    MenuWidget->AddToViewport();
} 
