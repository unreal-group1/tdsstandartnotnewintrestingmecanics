// SMOG STU Game, All Rights Reserved.


#include "Menu/UI/STUMenuWidget.h"
#include "STUGameInstance.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

DEFINE_LOG_CATEGORY_STATIC(LogSTUMenuWidget, All, All);

void USTUMenuWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (StartGameButton)
    {
        StartGameButton->OnClicked.AddDynamic(this, &USTUMenuWidget::OnStartGame);
    }

    if (QuitGameButton)
    {
        QuitGameButton->OnClicked.AddDynamic(this, &USTUMenuWidget::OnQuitGame);
    }
}

void USTUMenuWidget::OnStartGame()
{
    if (!GetWorld())
    {
        return;
    }
    const auto STUGameInstatnce = GetWorld()->GetGameInstance<USTUGameInstance>();
    if (!STUGameInstatnce)
    {
        return;
    }
    if (STUGameInstatnce->GetStartupLevelName().IsNone())
    {
        UE_LOG(LogSTUMenuWidget, Error, TEXT("Level name is NONE"));
        return;
    }

    UGameplayStatics::OpenLevel(this, STUGameInstatnce->GetStartupLevelName());
}

void USTUMenuWidget::OnQuitGame()
{
    UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(),EQuitPreference::Quit, true);
}
